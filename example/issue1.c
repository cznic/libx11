// +build ignore

#include <X11/Xlib.h>
#include <stdio.h>

int handler(Display *, XErrorEvent *) {
	printf("error\n");
}
 
int main(void) {
   Display *d;
   XEvent e;

   XSetErrorHandler(handler);
   d = XOpenDisplay(NULL);
   XMapWindow(d, 17);
   XNextEvent(d, &e);
   return 0;
}

