// Code generated for linux/amd64 by 'ccgo issue1.c -lX11', DO NOT EDIT.

//go:build linux && amd64

package main

import (
	"fmt"
	"unsafe"

	"modernc.org/libX11"
	"modernc.org/libc"
)

func handler(tls *libc.TLS, p0 uintptr, p1 uintptr) (r int32) {
	fmt.Println("error")
	return r
}

func main1(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	bp := tls.Alloc(int(unsafe.Sizeof(libx11.TXEvent{})))
	defer tls.Free(192)
	var d uintptr
	libx11.XXSetErrorHandler(tls, __ccgo_fp(handler))
	d = libx11.XXOpenDisplay(tls, 0)
	libx11.XXMapWindow(tls, d, 17)
	libx11.XXNextEvent(tls, d, bp)
	return 0
}

func main() {
	libc.Start(main1)
}

func __ccgo_fp(f interface{}) uintptr {
	type iface [2]uintptr
	return (*iface)(unsafe.Pointer(&f))[1]
}
