// Copyright 2024 The libx11-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libx11 // import "modernc.org/libx11"

import (
	"os"
	"runtime"
	"testing"

	_ "modernc.org/cc/v4"         // generator.go
	_ "modernc.org/ccgo/v4/lib"   // generator.go
	_ "modernc.org/fileutil/ccgo" // generator.go
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func Test(t *testing.T) {
	t.Log("TODO")
}
