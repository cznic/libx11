module modernc.org/libX11

go 1.20

require (
	modernc.org/cc/v4 v4.21.4
	modernc.org/ccgo/v4 v4.20.7
	modernc.org/fileutil v1.3.0
	modernc.org/libc v1.59.7
	modernc.org/libxcb v0.10.0
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/mod v0.19.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.22.0 // indirect
	golang.org/x/tools v0.23.0 // indirect
	modernc.org/gc/v2 v2.5.0 // indirect
	modernc.org/libXau v0.9.0 // indirect
	modernc.org/libXdmcp v0.11.0 // indirect
	modernc.org/libbsd v0.11.0 // indirect
	modernc.org/libmd v0.12.0 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/memory v1.8.0 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/sortutil v1.2.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
